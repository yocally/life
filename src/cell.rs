use saki::{Displayable, Texman};
use sdl2::rect::Rect;

#[derive(Clone, Copy, PartialEq)]
pub struct Cell {
	x: usize,
	y: usize,
	alive: bool,
}

impl Cell {
	pub fn new(x: usize, y: usize, alive: bool) -> Cell {
		Cell {
			x: x,
			y: y,
			alive: alive,
		}
	}

	pub fn alive(&self) -> bool {
		self.alive
	}

	pub fn set_alive(&mut self, alive: bool) {
		self.alive = alive;
	}

	pub fn x(&self) -> usize {
		self.x
	}

	pub fn y(&self) -> usize {
		self.y
	}
}

impl Displayable for Cell {
	fn paint(&self, texman: &mut Texman) {
		if self.alive {
			texman.draw(
				"cell",
				Rect::new(self.x as i32 * 8, self.y as i32 * 8, 8, 8),
			);
		} else {
			texman.draw(
				"cell_dead",
				Rect::new(self.x as i32 * 8, self.y as i32 * 8, 8, 8),
			);
		}
	}
}

extern crate sdl2;
extern crate saki;
extern crate rand;

mod cell;
mod grid;

use saki::*;
use sdl2::pixels::Color;
use sdl2::event::Event;
use sdl2::keyboard::Keycode;
use sdl2::rect::Rect;
use std::cell::RefCell;


fn main() {
	let (context, _, canvas, creator) = gen_context("Life", 570, 560);
	let mut texman = Texman::new(canvas, &creator);
	texman.load_image("assets/cell.png", "cell");
	texman.load_image("assets/cell_dead.png", "cell_dead");
	texman.load_image("assets/tools.png", "tools");
	texman.get_canvas().set_draw_color(
		Color::RGB(255, 255, 255),
	);
	texman.get_canvas().clear();
	texman.get_canvas().present();

	let grid = RefCell::new(grid::Grid::new(70));
	grid.borrow_mut().populate(600);

	let mut tools_button = Button::new("tools", "tools", Rect::new(560, 0, 10, 10));
	tools_button.connect(|| { grid.borrow_mut().populate(600); });

	let mut pal = Pallet::new();
	pal.insert("tools_button", tools_button);
	let mut event_pump = context.event_pump().unwrap();

	'main: loop {
		let state = event_pump.mouse_state();
		for event in event_pump.poll_iter() {
			match event {
				Event::Quit { .. } => {
					break 'main;
				}
				Event::KeyDown { keycode: Some(Keycode::Escape), .. } => {
					break 'main;
				}
				_ => {}
			}

			pal.update(&event, state.x(), state.y());
		}
		grid.borrow_mut().update();

		texman.get_canvas().clear();
		grid.borrow_mut().paint(&mut texman);
		pal.paint(&mut texman);
		texman.get_canvas().present();
	}
}

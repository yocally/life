use cell::Cell;
use saki::{Texman, Displayable};
use std::vec::Vec;
use rand::{Rng, thread_rng};


pub struct Grid {
	size: usize,
	grid: Vec<Vec<Cell>>,
}

impl Grid {
	pub fn new(size: usize) -> Grid {
		let mut rows = vec![vec![Cell::new(0, 0, false); size]; size];

		for x in 0..size {
			for y in 0..size {
				rows[x][y] = Cell::new(x, y, false);
			}
		}

		Grid {
			size: size,
			grid: rows,
		}
	}

	pub fn valid(&self, x: usize, y: usize) -> bool {
		x < self.grid.len() && y < self.grid[0].len()
	}

	pub fn set_state(&mut self, x: usize, y: usize, state: bool) {
		if self.valid(x, y) {
			self.grid[x][y].set_alive(state);
		} else {
			panic!("Tried to access a cell outside of the grid's size");
		}
	}

	pub fn clear(&mut self) {
		for x in 0..self.size {
			for y in 0..self.size {
				self.set_state(x, y, false);
			}
		}
	}

	pub fn populate(&mut self, volume: i32) {
		self.clear();
		for _ in 0..volume {
			loop {
				let x = thread_rng().gen_range(0, self.size);
				let y = thread_rng().gen_range(0, self.size);
				if !self.grid[x][y].alive() {
					self.set_state(x, y, true);
					break;
				}
			}
		}
	}

	pub fn neighbors(&self, target: &Cell) -> Vec<Cell> {
		let mut ret: Vec<Cell> = Vec::new();

		if self.valid(target.x(), target.y()) {
			let places = [
				(-1, -1),
				(-1, 0),
				(-1, 1),
				(0, 1),
				(0, -1),
				(1, 1),
				(1, -1),
				(1, 0),
			];
			for &(x, y) in &places {
				let xx = (target.x() as i32 + x) as usize;
				let yy = (target.y() as i32 + y) as usize;
				if self.valid(xx, yy) {
					ret.push(Cell::new(xx, yy, self.grid[xx][yy].alive()));
				}
			}
		}
		ret
	}

	pub fn alive_neighbors(&self, target: &Cell) -> Vec<Cell> {
		let mut ret: Vec<Cell> = Vec::new();
		let cmp = self.neighbors(target);
		for x in cmp {
			if x.alive() {
				ret.push(x);
			}
		}
		ret
	}

	pub fn update(&mut self) {
		let mut add = vec![];
		let mut remove = vec![];
		for x in 0..self.size {
			for target in self.grid[x].clone() {
				let live = self.alive_neighbors(&target);

				if live.len() != 2 && live.len() != 3 {
					remove.push(target);
				}
				if live.len() == 3 {
					add.push(target);
				}
			}
		}

		for target in add {
			self.set_state(target.x(), target.y(), true);
		}
		for target in remove {
			self.set_state(target.x(), target.y(), false);
		}
	}
}

impl Displayable for Grid {
	fn paint(&self, texman: &mut Texman) {
		for x in &self.grid {
			for y in x {
				y.paint(texman);
			}
		}
	}
}
